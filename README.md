# twiliobot

## Getting Started with Twilio
Go to [Twilio](https://www.twilio.com/try-twilio) and set up an account. There is a trial tier (read the instructions as they may change over time).

## Read up on Twilio

[Twilio tutorial for 2-way messaging](https://www.twilio.com/blog/2013/10/test-your-webhooks-locally-with-ngrok.html)

[Inbound numbers](https://www.twilio.com/console/phone-numbers/incoming) where you set the webhook after firing up ngrok

[Review your logs](https://www.twilio.com/console/sms/logs)

## npm
You should just be able to run `npm install`. Below are the pieces that were imported during development
```npm install```

### Skip this: just for reference for what makes up this project
Install twilio to your project
```npm install twilio```

express
```npm install express```

dotenv - place for private info like API keys stored in .env
[Article](https://codeburst.io/process-env-what-it-is-and-why-when-how-to-use-it-effectively-505d0b2831e7)
```npm install dotenv --save```

[nodemon](https://www.npmjs.com/package/nodemon) to live reload the server
```npm install --save-dev nodemon```

run nodemon
```npx nodemon index.js```

## ngrok
This is where the magic happens as described in the Twilio tutorial referenced earlier.

Install on a Mac is as easy as `brew cask install ngrok`. It should be on all the other common Linux package managers.

If your node instance is running on port 8900 (e.g., .env.PORT=8900), bind ngrok to that port.

```ngrok http 8900```

Look for the forwarding address (e.g., https://55c17a8f.ngrok.io).

Set that address in Inbound Numbers referenced above in your Twilio configuration.

