const process = require('process');
require('dotenv').config();
var express = require("express");
var app = express();
app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

var twilio = require("twilio"); 
var accountSid = process.env.SID;
var authToken = process.env.TOKEN;
var client = new twilio(accountSid, authToken);

// respond with "hello world" when a GET request is made to the homepage
app.get("/", function(req, res) {
  res.send("GET / called");
});

app.post("/inbound", function(req, res) {
  console.log(req.body);
  client.messages
    .create({
      body: "ECHO: " + req.body.Body,
      to: req.body.From, 
      from: process.env.TWPHONE // From a valid Twilio number
    })
    .then(message => res.send(message));
});

var listener = app.listen(process.env.PORT, function() {
  console.log("Your app is listening on port " + listener.address().port);
});
